from django.contrib import admin

# Register your models here.

from .models import (
    DogModel,
)

@admin.register(DogModel)
class DogAdmin(admin.ModelAdmin):
    list_display = ('name', 'breed','owner','age')
    search_fields = ('name', )

