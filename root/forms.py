from django import forms

from .models import DogModel

class PostForm(forms.ModelForm):

    class Meta:
        model = DogModel
        fields = ('name', 'breed', 'owner', 'age')