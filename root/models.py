from django.db import models

# Create your models here.


class DogModel(models.Model):
    name = models.CharField(max_length=200, unique=True)
    breed = models.CharField(max_length=200, unique=True)
    owner = models.CharField(max_length=200, unique=True)
    age = models.IntegerField(verbose_name=('age'))
    
    def __str__(self):
        return self.name
