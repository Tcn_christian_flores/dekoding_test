from django.urls import path

from .views import (
    IndexView,
    FrontendView,
    BackendView,
    DogAddView,
    DogListView,
    SlidesView,
    RankanaView,
)


urlpatterns = [
    path('', IndexView.as_view(), name="index"),
    path('frontend/', FrontendView.as_view(), name="frontend"),
    path('backend/', BackendView.as_view(), name="backend"),
    path('dog/list', DogListView.as_view(), name="dog_list"),
    path('dog/add', DogAddView.as_view(), name="dog_add"),
    path('slides', SlidesView.as_view(), name="slides"),
    path('rankana', RankanaView.as_view(), name="rankana"),
]
