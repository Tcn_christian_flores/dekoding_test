from django.shortcuts import render
from django.views.generic import View
from .models import DogModel
from .forms import PostForm

class IndexView(View):
    template_name = "index.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class FrontendView(View):
    template_name = "frontend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class BackendView(View):
    template_name = "backend.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)


class DogAddView(View):
    template_name = "add.html"

    def get(self, request):
        name = request.GET.get("name")
        age = request.GET.get("age")
        breed = request.GET.get("breed")
        owner = request.GET.get("owner")
        context = {}

        return render(request, self.template_name, context)
    def post(self, request):
        print('POST')
        if request.method == "POST":
            form = PostForm(request.POST)
            if form.is_valid():
                post = form.save(commit=False)
                post.save()
               # return redirect('blog.views.post_detail', pk=post.pk)
        else:
            form = PostForm()
        return render(request, 'add.html', {'form': form})
        #return render(request, 'blog/post_edit.html', {'form': form})   

class DogListView(View):
    template_name = "list.html"
    
    def get(self, request):
        all_dogs = DogModel.objects.all()
        print(all_dogs)
        context = {
            'dogs': all_dogs,
        }

        return render(request, self.template_name, context)


class SlidesView(View):
    template_name = "slides.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)
        
class RankanaView(View):
    template_name = "rankana.html"

    def get(self, request):
        context = {}

        return render(request, self.template_name, context)
        